import logging

import pandas as pd
from dwclib.common.db import pguri
from sqlalchemy import MetaData, Table, create_engine, select


def build_labgds_tasks(patients, config, outroot):
    if "labgds" not in config["extract"]:
        return []
    tasks = []
    for p in patients:
        dwcid = p.name
        ipp = p["lifetimeid"]
        if not ipp:
            continue
        labgdsdir = outroot / dwcid / "labgds"
        labgdsdir.mkdir(parents=True, exist_ok=True)
        tasks.append((ipp, labgdsdir))
    return [(labgds_extract, part) for part in tasks]


def labgds_extract(ipp, outfolder):
    logger = logging.getLogger("default")

    engine = create_engine(pguri.set(database="lrbdata"))
    pgmeta = MetaData(schema="kafka")
    pgmeta.reflect(engine)

    t_labgds = Table("labgds", pgmeta, autoload_with=engine)
    labt = t_labgds.c

    outfile = outfolder / "labgds.parquet"
    outfile_placeholder = outfolder / f".{outfile.name}.empty"

    with engine.connect() as conn:
        q = select([labt.datetime, labt.specimen_type, labt.label, labt.value])
        q = q.where(labt.ipp == ipp)
        logger.debug(q.compile(engine))
        df = pd.read_sql(q, conn, index_col="datetime")

    if len(df) < 1:
        outfile_placeholder.touch()
        return
    df.to_parquet(outfile, coerce_timestamps="us", allow_truncated_timestamps=True)
