import pandas as pd

from .extract import run_extraction
from .tasks.enumerations import get_enumerations
from .tasks.numerics import get_numerics
from .tasks.waves import get_waves


def build_dwc_tasks(patients, config, outroot):
    tasks = []
    timedelta = pd.to_timedelta(config["extract"]["interval"])
    for p in patients:
        if "dwcenumerations" in config["extract"]:
            parts = get_enumerations(
                p.name,
                timedelta,
                outroot,
                p.data_begin,
                p.data_end,
                config["extract"]["dwcenumerations"],
            )
            tasks += [(run_extraction, part) for part in parts]

        if "dwcnumerics" in config["extract"]:
            parts = get_numerics(
                p.name,
                timedelta,
                outroot,
                p.data_begin,
                p.data_end,
                config["extract"]["dwcnumerics"],
            )
            tasks += [(run_extraction, part) for part in parts]

        if "dwcwaves" in config["extract"]:
            parts = get_waves(
                p.name,
                timedelta,
                outroot,
                p.data_begin,
                p.data_end,
                config["extract"]["dwcwaves"],
            )
            tasks += [(run_extraction, part) for part in parts]
    return tasks
