import logging
from pathlib import Path
from typing import List, Optional

import pandas as pd


def run_extraction(
    func,
    namespace,
    dwcid: str,
    outfolder: Path,
    data_begin: pd.Timestamp,
    data_end: pd.Timestamp,
    labels: Optional[List[str]] = None,
):
    logger = logging.getLogger("default")
    logger.info(
        f"{dwcid}, {namespace}: {data_begin.date()} {data_begin.time()} - {data_end.time()}"
    )
    outfile = outfolder / f"{data_begin}.parquet"
    outfile_placeholder = outfolder / f".{outfile.name}.empty"
    if outfile.exists() or outfile_placeholder.exists():
        logger.debug(f"Skipping existing {outfile}")
        return
    df = func(dwcid, data_begin, data_end, labels)
    if len(df) < 1:
        outfile_placeholder.touch()
        return
    df.to_parquet(outfile, coerce_timestamps="us", allow_truncated_timestamps=True)
