from math import ceil
from pathlib import Path
from typing import List, Optional

import pandas as pd
from dwclib import read_waves


def get_waves(
    dwcid: str,
    timedelta: pd.Timedelta,
    outfolder: Path,
    data_begin: pd.Timestamp,
    data_end: pd.Timestamp,
    labels: Optional[List[str]] = None,
):
    patientdir = outfolder / dwcid
    patientdir.mkdir(exist_ok=True)
    wavdir = patientdir / "dwcwaves"
    wavdir.mkdir(exist_ok=True)

    if data_begin is None or data_end is None:
        return []

    npartitions = int(ceil((data_end - data_begin) / timedelta))

    params = []
    for i in range(npartitions):
        beg_time = data_begin + i * timedelta
        end_time = beg_time + timedelta
        params.append(
            (read_waves, "dwcwaves", dwcid, wavdir, beg_time, end_time, labels)
        )
    return params
