from configparser import ConfigParser, NoSectionError
from pathlib import Path

from platformdirs import user_config_dir

config_dir = Path(user_config_dir(appname="larib-data", appauthor="larib-data"))
configfile = config_dir / "config.ini"

configuration = {}

if configfile.exists():
    config = ConfigParser()
    config.read(configfile)
    try:
        configuration["nas_mountpoint"] = config.get("nas", "mountpoint")
    except NoSectionError:
        configuration["nas_mountpoint"] = ""
